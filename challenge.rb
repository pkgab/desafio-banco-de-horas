require 'json'
require 'date'

file = File.read(ARGV[0])

object =  JSON.parse(file)

employees = []

object['employees'].each_with_index do | employee, index | 
    workload = employee['workload']
    
    # Ordenando as datas em ordem crescente
    employee['entries'] = employee['entries'].sort_by{|d| y,m,d=d.split("-");[y,m,d]}

    # Parseando para o tipo DateTime para trabalhar posteriormente
    employee['entries'].each_with_index do | entrie, index |
        employee['entries'][index] =  DateTime.parse(entrie)
    end

    # Dividindo os horários de cada dia para trabalhar com os horário em par
    employee['entries'] = employee['entries'].each_slice(2).to_a

    minutesWorked = {}
    totalHours = 0

    # Each para contar quantos minutos o funcionário trabalhou e verificando se trabalhou a mais ou a menos no dia
    employee['entries'].each do | date |
        key = "#{date[0].day}/#{date[0].month}/#{date[0].year}"

        if(minutesWorked.key?(key))
            work = minutesWorked[key][0]['worked_minutes'] + ((date[1] - date[0]) * 24 * 60 * 60).to_i / 60
            minutesWorked[key] = ['worked_minutes' => work]
        else
            minutesWorked[key] = ['worked_minutes' => ((date[1] - date[0]) * 24 * 60 * 60).to_i / 60]
        end

        workBalace = workload['workload_in_minutes'] - minutesWorked[key][0]['worked_minutes']

        if(workBalace < 0)
            minutesWorked[key].push({'minutes_more_worked' => workBalace.abs})
        else
            minutesWorked[key].push({'minutes_less_worked' => workBalace})
        end

        totalHours += minutesWorked[key][0]['worked_minutes']
    end

    balanceOfHours = 0

    # Pegando o saldo de horas através do saldo de cada dia
    minutesWorked.each do | time |
        if(time[1][1]['minutes_more_worked']) 
            balanceOfHours += time[1][1]['minutes_more_worked']
        else
            balanceOfHours -= time[1][1]['minutes_less_worked']
        end
    end

    employees.push({'name' => employee['name'], 'time' => minutesWorked, 'balance' => balanceOfHours})
end

employees.each do | employee |
    puts "Employee name: #{employee['name']}"

    employee['time'].each do | timeWorked |
        puts "On #{timeWorked[0]} worked #{timeWorked[1][0]['worked_minutes']} minutes. #{timeWorked[1][1]}"
    end

    puts "Balance in minutes: #{employee['balance']}"

    puts "\n"
end 